<html>

<body>

    <p>Run <kbd>NOTIFY dml_events, 'some message';</kbd> in Postgres (either in the <code>postgres</code> database or whatever you have set for <code>$PGDATABASE</code>)</p>

    <div id="out"></div>

  <script src="//cdnjs.cloudflare.com/ajax/libs/sockjs-client/0.3.4/sockjs.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.min.js"></script>

  <script>

      var socket = new SockJS("/hello");

      var stompClient = Stomp.over(socket);

      stompClient.connect( {}, function(frame) {

        stompClient.subscribe("/channels/dml_events", function(response) {
            document.getElementById("out").innerText += response + "\r\n\r\n";
        });

      });

  </script>
</body>

</html>