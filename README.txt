You need git and maven installed to run this.

You may need to set some Environment Variables first[1]

In your command prompt, run:

git clone https://bitbucket.org/neilmcg/postgresql-websocket-example.git && cd postgresql-websocket-example && mvn tomcat7:run

then open your web browser to http://localhost:8080


[1] By default, the program uses the following environment variables:

PGHOST (defaults to localhost)
PGPORT (default to 5432)
PGDATABASE (defaults to postgres)
PGUSER (defaults to postgres)

If you don't have PGPASSWORD set, in your command prompt run:

set PGPASSWORD=s3cr3t

where s3cr3t is the password for your postgres user. 